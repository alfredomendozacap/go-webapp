package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/alfredomendozacap/go-webapp/models"

	"gitlab.com/alfredomendozacap/go-webapp/routes"
	"gitlab.com/alfredomendozacap/go-webapp/utils"
)

// PORT is the port for the app
var PORT = ":8080"

func init() {
	utils.LoadTemplates("views/*.html")
}

func main() {
	if os.Getenv("PORT") != "" {
		PORT = ":" + os.Getenv("PORT")
	}
	r := routes.NewRouter()

	models.TestConnection()

	fmt.Printf("Listening Port %s\n", PORT)
	log.Fatal(http.ListenAndServe(PORT, r))
}
