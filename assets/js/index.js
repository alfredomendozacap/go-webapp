;
((d, c)=>{
  // Materialize components
  M.AutoInit()


  // Input Search
  const inputSearch = d.getElementById('search'),
        productsBody = d.getElementById('productsBody')

  inputSearch.addEventListener('keyup', async e => {
    let search = e.target.value
    const res = await axios.post('/', { search })
    if (res.data) {
      M.Toast.dismissAll()
      productsBody.innerHTML = res.data
    } else {
      productsBody.innerHTML = ''
      M.toast({ html: `<b>No se encontró nada con <code>${search}</code></b>`, classes: 'red darken-1'})
    }
  })
})(document, console.log)