package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	// pq is the driver for postgreSQL
	_ "github.com/lib/pq"
)

type dbParameters struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     string `json:"port"`
	DBName   string `json:"dbname"`
}

func getConfiguration() dbParameters {
	dbparams := dbParameters{}
	config, err := ioutil.ReadFile("./config.json")
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(config, &dbparams)
	if err != nil {
		log.Fatalf("Cannot parse config.json file: %s\n", err)
	}
	return dbparams
}

// GetConnection returns the connection to the DB
func GetConnection() *sql.DB {
	dsn := os.Getenv("DATABASE_URL")
	if dsn == "" {
		dbparams := getConfiguration()
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable&client_encoding=utf8",
			dbparams.User,
			dbparams.Password,
			dbparams.Host,
			dbparams.Port,
			dbparams.DBName,
		)
	}
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}

	return db
}

// TestConnection tests the DB connection
func TestConnection() {
	con := GetConnection()
	defer con.Close()

	err := con.Ping()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Database connected successfully")
}
