package models

// User is the struct for users
type User struct {
	ID        uint64 `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	Status    string `json:"status"`
}

// NewUser creates a new user
func NewUser(u User) (bool, error) {
	db := GetConnection()
	defer db.Close()

	query := "INSERT INTO users (first_name, last_name, email, password) VALUES ($1, $2, $3, $4)"
	stmt, err := db.Prepare(query)
	if err != nil {
		return false, err
	}
	defer stmt.Close()

	_, err = stmt.Exec(u.FirstName, u.LastName, u.Email, u.Password)
	if err != nil {
		return false, err
	}
	return true, nil
}
