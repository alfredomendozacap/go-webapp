package models

// Product is the struct for products
type Product struct {
	ID       uint64
	Name     string
	Price    float64
	Quantity int
	Amount   float64
	Category Category
}

// GetProducts gets all the products from the DB
func GetProducts(search string) ([]Product, error) {
	db := GetConnection()
	defer db.Close()

	query := `SELECT c.id,
			c.description,
			p.id, p.name, p.price,
			p.quantity, p.amount
		FROM products p INNER JOIN category c
		ON c.id = p.category
		WHERE lower(p.name) LIKE lower('%'||$1||'%')
		ORDER BY p.id ASC`
	rows, err := db.Query(query, search)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var products []Product
	for rows.Next() {
		product := Product{}
		err = rows.Scan(
			&product.Category.ID,
			&product.Category.Description,
			&product.ID,
			&product.Name,
			&product.Price,
			&product.Quantity,
			&product.Amount,
		)
		if err != nil {
			return nil, err
		}
		products = append(products, product)
	}
	return products, nil
}
