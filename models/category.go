package models

// Category is the struct for category
type Category struct {
	ID          int
	Description string
}

// GetCategories gets all the categories from the DB
func GetCategories() ([]Category, error) {
	db := GetConnection()
	defer db.Close()

	query := "SELECT id, description FROM category ORDER BY id ASC"
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var categories []Category
	for rows.Next() {
		category := Category{}
		err = rows.Scan(&category.ID, &category.Description)
		if err != nil {
			return nil, err
		}
		categories = append(categories, category)
	}
	return categories, nil
}
