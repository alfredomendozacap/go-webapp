create database macaco template template0 lc_collate='es-PE-x-icu' lc_ctype='es-PE-x-icu' owner "angel" tablespace "tbls_tutorials";

\c macaco;

create table if not exists category (
    id serial primary key,
    description varchar(100) not null
);

create table if not exists products (
    id bigserial primary key,
    name varchar(255) not null,
    price real not null,
    quantity integer default 0,
    amount real default 0.0,
    category bigint not null,
    constraint products_category_fk foreign key (category)
    references category (id)
);

create table if not exists users(
    id bigserial,
    first_name varchar(15) not null,
    last_name varchar(20) not null,
    email varchar(100) not null,
    password varchar(100) not null,
    status char(1) default '0',
    constraint users_pk primary key (id),
    constraint users_email_uk unique (email)
);