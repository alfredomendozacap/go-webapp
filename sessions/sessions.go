package sessions

import (
	"net/http"
	"os"

	"github.com/gorilla/sessions"
)

func setKeyPairs() string {
	keyPairs := os.Getenv("KEY_PAIRS")
	if keyPairs == "" {
		keyPairs = "S3CR3TK3Y"
	}
	return keyPairs
}

// Store is a CookieStore
var Store = sessions.NewCookieStore([]byte(setKeyPairs()))

// Flash deletes and returns a flash message
func Flash(r *http.Request, w http.ResponseWriter) string {
	session, _ := Store.Get(r, "flash-session")
	untypedMessage := session.Values["MESSAGE"]
	if message, ok := untypedMessage.(string); ok {
		delete(session.Values, "MESSAGE")
		session.Save(r, w)
		return message
	}
	return ""
}
