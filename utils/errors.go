package utils

import "net/http"

// InternalServerError displays an internal error message
func InternalServerError(w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("Oops! Ocurrio un error interno. :("))
}
