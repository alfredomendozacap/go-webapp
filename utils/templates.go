package utils

import (
	"html/template"
	"net/http"
)

var templates *template.Template

// LoadTemplates parses all the html templates by a path
func LoadTemplates(path string) {
	templates = template.Must(template.ParseGlob(path))
}

// ExecuteTemplate executes a html template by its filename
func ExecuteTemplate(w http.ResponseWriter, tmpl string, data interface{}) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	templates.ExecuteTemplate(w, tmpl, data)
}
