package utils

import (
	"encoding/json"
	"net/http"
)

// ToJSON encodes data to JSON
func ToJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(data)
}
