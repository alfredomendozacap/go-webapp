package routes

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/alfredomendozacap/go-webapp/models"

	"gitlab.com/alfredomendozacap/go-webapp/utils"
)

func homeGetHandler(w http.ResponseWriter, r *http.Request) {
	categories, err := models.GetCategories()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error"))
	}

	products, err := models.GetProducts("")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error"))
	}

	utils.ExecuteTemplate(w, "home.html", struct {
		Categories []models.Category
		Products   []models.Product
	}{
		Categories: categories,
		Products:   products,
	})
}

func homePostHandler(w http.ResponseWriter, r *http.Request) {
	type form struct {
		Search string `json:"search"`
	}
	search := form{}
	err := json.NewDecoder(r.Body).Decode(&search)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error"))
		return
	}

	products, err := models.GetProducts(search.Search)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error"))
		return
	}

	var html string
	if len(products) > 0 {
		for _, product := range products {
			html += fmt.Sprintf(`
				<tr>
				  <th scope="row">%v</th>
				  <td>%s</td>
				  <td>%s</td>
				  <td>%v</td>
				  <td>%v</td>
				  <td>%v</td>
				</tr>
			`,
				product.ID,
				product.Category.Description,
				product.Name,
				product.Price,
				product.Quantity,
				product.Amount,
			)
		}
	}

	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	w.Write([]byte(html))
}
