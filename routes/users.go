package routes

import (
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/alfredomendozacap/go-webapp/sessions"

	"gitlab.com/alfredomendozacap/go-webapp/models"

	"gitlab.com/alfredomendozacap/go-webapp/utils"
)

func registerGetHandler(w http.ResponseWriter, r *http.Request) {
	message := sessions.Flash(r, w)
	utils.ExecuteTemplate(w, "register.html", struct{ Message template.HTML }{Message: template.HTML(message)})
}

func registerPostHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := models.User{}
	user.FirstName = r.PostForm.Get("firstName")
	user.LastName = r.PostForm.Get("lastName")
	user.Email = r.PostForm.Get("email")
	user.Password = r.PostForm.Get("password")
	fmt.Println(user)
	_, err := models.NewUser(user)
	if err != nil {
		utils.InternalServerError(w)
		return
	}

	session, _ := sessions.Store.Get(r, "flash-session")
	session.Values["MESSAGE"] = "<script>M.toast({html: 'Registrado con Éxito!', classes: 'teal lighten-1'})</script>"
	session.Save(r, w)
	http.Redirect(w, r, "/register", http.StatusSeeOther)
}
